# README #

Small ksh script to generate a wireguard mesh network config with/for OpenBSD.

### Usage ###

    ./wgenconf.sh doma.in port host1 host2 host3 host4 ...

as in:

    ./wgenconf.sh aroundtheglobe.biz 23555 lu be fr es

Would generate the files for 4 hosts:
 
1. lu.aroundtheglobe.biz - as server for be,fr,es

2. be.aroundtheglobe.biz - as client to lu and server to fr,es

3. fr.aroundtheglobe.biz - as client to lu,be, and server to es

4. es.aroundtheglobe.biz - as client to lu,be,fr

**wgenconf is made to work with domain names, not IP.  If you have dynamic IP and a domain name, you can use ddclient.**

### Status ###

[V] : Generate mesh config

[V] : Generate hostname.wg*

[V] : Generate wireguard/wg*.conf

[V] : Generate routing rules

[V] : Generate pf entries

[=~] : Generate dnsentries

[X] : Add mobile clients (done manually at the moment)

### From here ###

I have quite some things to do these times, I plan to eventually add the missing features one day, but the current state is sufficient for my daily use.
