#!/bin/ksh
# Copyright 2021 Nelson-Jean Gaasch
# Licence: (New BSD Licence)
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#######################
# version 1.0 - beta5 #
#######################
set -A args -- "$@"
set -A nodes
def='\033[0m'; red='\033[1;31m'; gre='\033[1;32m'; yel='\033[1;33m'; blu='\033[1;34m'; cya='\033[1;36m'; bol='\033[1m'
if [[ ! -f /usr/local/bin/wg ]]; then printf "You need to %bpkg_add wireguard-tools%b first\n" "$red" "$def" && exit; fi
### Display usage if asked for or if no arguments are passed
if [[ "${args[*]}" = "" ]] || [[ "${args[0]}" = "--help" ]] || [[ "${args[0]}" = "help" ]]; then printf "Usage is:\n./wgenconf.sh domainname port node1 node2 ... nodeX\n" && exit; fi
### Parse argumentss
a=0
for arg in "${args[@]}"; do
	if [[ "$a" = "0" ]]; then domain="$arg"
	elif [[ "$a" = "1" ]]; then pport="$arg"
	else set -A nodes "${nodes[@]}" "$arg"
	fi
	a=$((a+1))
done
### Check that parsing is valid and correct.
nodecount=${#nodes[@]} && if [[ "$nodecount" -gt "9" ]]; then echo "More than 9 nodes, this will not work as is... Aborting" && exit; fi
if [[ "$(echo "${nodes[@]}"|tr " " "\n"|sort -u|wc -l|tr -d ' ')" != "$nodecount" ]]; then echo "Same node detected twice: aborting" && exit; fi
printf "You would like to generate certificates for: %b%s%b on domain %b%s%b - with service running on port %b%s%b\nType %byes%b if this is correct\n" "$red" "${nodes[*]}" "$def" "$blu" "$domain" "$def" "$cya" "$pport" "$def" "$gre" "$def";
read -r aanswer && if [[ "$aanswer" != "yes" ]]; then echo "aborting" && exit; fi
if [[ -d "$domain" ]]; then
	aanswer=""
	printf "%b%s%b folder already exists: type yes to replace it.%b\n" "$bol" "$domain" "$red" "$def"
	read -r aanswer
	if [[ "$aanswer" != "yes" ]]; then
		echo "aborting" && exit
	else
		rm -Rf "$domain"
	fi
fi
### Generate config
servcount=1
for servnode in "${nodes[@]}"; do
	clientcount=1
	servprivkey="$(wg genkey)" && servpubkey="$(echo "$servprivkey"|wg pubkey)"
	mkdir -p "$domain/$servnode.$domain/wireguard"
	printf "[Interface]\n PrivateKey = %s\n ListenPort = %s\n" "$servprivkey" "$pport"> "$domain/$servnode.$domain/wireguard/wg0.conf"
	printf "inet 10.0.%s.1 255.255.255.0 NONE\nup\n!/usr/local/bin/wg setconf wg0 /etc/wireguard/wg0.conf\n" "$servcount" > "$domain/$servnode.$domain/hostname.wg0"
	echo "net.inet.ip.forwarding=1" > "$domain/$servnode.$domain/sysctl.conf"
	i=0;set -A pfwgarray && while [[ "$i" -lt "$servcount" ]]; do set -A pfwgarray "${pfwgarray[@]}" "wg$i" && i=$((i+1)); done
	printf "VPN=\"%s\"\npass in quick on { \$TERNET1 } inet proto udp from any to { (\$TERNET1) } port %s\npass in quick on { \$VPN } from any to any\npass out quick on { \$VPN } from any to any\n" "${pfwgarray[*]}" "$pport" >> "$domain/$servnode.$domain/pf.add"
	echo "10.0.$servcount.$servcount         $servnode.$servnode.$domain" >> "$domain/unbound.txt"
	for clientnode in "${nodes[@]}"; do
		mkdir -p "$domain/$clientnode.$domain/wireguard"
		if [[ "$servcount" -ge "$clientcount" ]]; then
			clientcount=$((clientcount+1))
			continue
		fi
		clientprivkey="$(wg genkey)" && clientpubkey="$(echo "$clientprivkey"|wg pubkey)" && pskey="$(wg genpsk)"
		printf "[Interface]\n PrivateKey = %s\n[Peer]\n PublicKey = %s\n Endpoint = %s.%s:%s\n AllowedIPs = 10.0.%s.0/24, 172.2%s.0.0/16\n PreSharedKey = %s\n PersistentKeepalive = 20\n" "$clientprivkey" "$servpubkey" "$servnode" "$domain" "$pport"  "$servcount" "$servcount" "$pskey"> "$domain/$clientnode.$domain/wireguard/wg$servcount.conf"
		printf "[Peer]\n PublicKey = %s\n AllowedIPs = 10.0.%s.%s/32, 172.2%s.0.0/16 \n PreSharedKey = %s\n" "$clientpubkey" "$servcount" "$clientcount" "$clientcount" "$pskey" >> "$domain/$servnode.$domain/wireguard/wg0.conf"
		printf "inet 10.0.%s.%s 255.255.255.0 NONE\nup\n!/usr/local/bin/wg setconf wg%s /etc/wireguard/wg%s.conf\n" "$servcount" "$clientcount" "$servcount" "$servcount" > "$domain/$clientnode.$domain/hostname.wg$servcount"
		printf "/sbin/route add -net 172.2%s.0.0/16 10.0.%s.1\n/sbin/route add -net 10.%s.0.0/16 10.0.%s.1\n/sbin/route add -net 192.168.%s.0/24 10.0.%s.1\n" "$servcount" "$servcount" "$servcount" "$servcount" "$servcount" "$servcount" >> "$domain/$clientnode.$domain/routing.sh"
		printf "/sbin/route add -net 172.2%s.0.0/16 10.0.%s.%s\n/sbin/route add -net 10.%s.0.0/16 10.0.%s.%s\n/sbin/route add -net 192.168.%s.0/24 10.0.%s.%s\n" "$clientcount" "$servcount" "$clientcount" "$clientcount" "$servcount" "$clientcount" "$clientcount" "$servcount" "$clientcount" >> "$domain/$servnode.$domain/routing.sh"
		clientcount=$((clientcount+1))
		echo "10.0.$servcount.$clientcount         $clientnode.$servnode.$domain" >> "$domain/unbound.txt"
	done
	servcount=$((servcount+1))
done
cd "$domain" && for ffolder in *."$domain"; do cp unbound.txt "$ffolder/" && tar -czf "$ffolder.tar.gz" "$ffolder" && rm -Rf "$ffolder"; done && rm unbound.txt && cd ..
printf "%bAll done, files are in %s %b\360\237\246\204%b\n" "$yel" "$domain" "$red" "$def"
